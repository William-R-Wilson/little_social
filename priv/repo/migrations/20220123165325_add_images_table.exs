defmodule LittleSocial.Repo.Migrations.AddImagesTable do
  use Ecto.Migration

  def change do
    create table(:images) do
      add :user_id, :integer
      add :image_url, :string
      timestamps()
    end
  end
end
