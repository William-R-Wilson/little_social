defmodule LittleSocial.Repo.Migrations.AddIndexes do
  use Ecto.Migration

  def change do
    create index("posts", :user_id)
    create index("comments", :post_id)
  end
end
