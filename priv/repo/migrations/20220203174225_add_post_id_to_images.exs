defmodule LittleSocial.Repo.Migrations.AddPostIdToImages do
  use Ecto.Migration

  # add post_id
  # also add filename since we can use this to find the image on s3 as well as the url
  # add indexes on user_id and post_id
  def change do
    alter table("images") do 
      add :post_id, references(:posts) 
      add :filename, :string
    end
    create index("images", :post_id)
    create index("images", :user_id)
  end
end
