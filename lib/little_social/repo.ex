defmodule LittleSocial.Repo do
  use Ecto.Repo,
    otp_app: :little_social,
    adapter: Ecto.Adapters.Postgres
end
