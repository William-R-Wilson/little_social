defmodule LittleSocial.Posts do
  @moduledoc """
  The Posts context.
  """

  import Ecto.Query, warn: false
  alias LittleSocial.Repo

  alias LittleSocial.Posts.Post
  alias LittleSocial.Images.Image
  alias LittleSocial.Posts.Comment

  @doc """
  Returns the list of posts.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts do
    q = from(p in Post, order_by: [desc: p.inserted_at])
    Repo.all(q)
    |> Repo.preload(:image)
    |> Repo.preload(:user)
    |> Repo.preload(:comments)
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id) do
    # look into this blog post to optimize this
    # https://thoughtbot.com/blog/preloading-nested-associations-with-ecto
    Repo.get!(Post, id)
    |> Repo.preload(:image)
    |> Repo.preload(:user)
    |> Repo.preload(comments: from(c in Comment, preload: :user, order_by: [desc: c.inserted_at]))
  end

  def list_comments(post) do 
    from(c in Comment, 
         where: [post_id: ^post.id],
         order_by: [asc: :id])
    |> Repo.all()
  end

  def create_comment(post, attrs \\ %{}) do
    post
    |> Ecto.build_assoc(:comments)
    |> Comment.changeset(attrs)
    |> Repo.insert()
  end

  def create_image(post, params) do
    unique_filename = "#{UUID.uuid4(:hex)}-#{params.filename}"
    bucket_name = System.get_env("BUCKET_NAME")  
    # TODO handle failure here
    _image = ExAws.S3.Upload.stream_file(params.path)
      |> ExAws.S3.upload(bucket_name, unique_filename)          
      |> ExAws.request!
    image_url = "https://#{bucket_name}.s3.amazonaws.com/#{bucket_name}/#{unique_filename}" 
    updated_params = %{ user_id: post.user_id, 
                        post_id: post.id,
                        filename: unique_filename,
                        image_url: image_url }  

    changeset = Image.changeset(%Image{}, updated_params)  
    Repo.insert(changeset)
  end

  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    Repo.delete(post)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{data: %Post{}}

  """
  def change_post(%Post{} = post, attrs \\ %{}) do
    Post.changeset(post, attrs)
  end
end
