require IEx

defmodule LittleSocial.Images do 
  import Ecto.Query, warn: false
  alias LittleSocial.Repo
  alias LittleSocial.Accounts.User
  alias LittleSocial.Images.Image


  def change_image(%Image{} = image, attrs \\ %{}) do
    Image.changeset(image, attrs)
  end

  def get_image!(id) do 
    img = Repo.get!(Image, id)
    bucket = System.get_env("BUCKET_NAME")
    config = ExAws.Config.new(:s3)
    ExAws.S3.presigned_url(config, :get, bucket, img.filename)
  end

  def presigned_url(img) do 
    bucket = System.get_env("BUCKET_NAME")
    config = ExAws.Config.new(:s3)
    case ExAws.S3.presigned_url(config, :get, bucket, img.filename) do
      {:ok, url} ->
        url
      {:error, _url} ->
        "error accessing image"
    end
  end

  def create_image(params, user_id) do 
    unique_filename = "#{UUID.uuid4(:hex)}-#{params.filename}"
    bucket_name = System.get_env("BUCKET_NAME")  
    # TODO handle failure here
    image = ExAws.S3.Upload.stream_file(params.path)
      |> ExAws.S3.upload(bucket_name, unique_filename)          
      |> ExAws.request!
    image_url = "https://#{bucket_name}.s3.amazonaws.com/#{bucket_name}/#{unique_filename}" 
    updated_params = %{ user_id: user_id, 
                        filename: unique_filename,
                        image_url: image_url }  

    changeset = Image.changeset(%Image{}, updated_params)  
    Repo.insert!(changeset)
  end

end
