defmodule LittleSocial.Posts.Post do
  use Ecto.Schema
  import Ecto.Changeset

  schema "posts" do
    field :body, :string
    field :title, :string
    belongs_to :user, LittleSocial.Accounts.User
    has_many :comments, LittleSocial.Posts.Comment
    has_one :image, LittleSocial.Images.Image
    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:body, :title, :user_id])
    |> cast_assoc(:comments)
    |> validate_required([:body, :user_id])
  end
end
