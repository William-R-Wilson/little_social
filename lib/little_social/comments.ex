defmodule LittleSocial.Comments do 
  @moduledoc """
  Comments Context
  """

  import Ecto.Query, warn: false
  alias LittleSocial.Repo
  alias LittleSocial.Accounts.User
  alias LittleSocial.Posts.Post
  alias LittleSocial.Posts.Comment

  def post_comments(post_id) do 
    query = from c in Comment,
            where: c.post_id == ^post_id,
            order_by: [desc: c.inserted_at]
    Repo.all(query)
    |> Repo.preload(:user)
  end

  def create_comment(attrs \\ %{}) do
    %Comment{}
    |> Comment.changeset(attrs)
    |> Repo.insert()
  end

end
