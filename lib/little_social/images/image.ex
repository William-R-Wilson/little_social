defmodule LittleSocial.Images.Image do 
  use Ecto.Schema
  import Ecto.Changeset

  schema "images" do 
    field :image_url, :string
    field :filename, :string
    belongs_to :user, LittleSocial.Accounts.User
    belongs_to :post, LittleSocial.Posts.Post
    timestamps()
  end

  @doc false
  def changeset(image, attrs) do 
    image
    |> cast(attrs, [:image_url, :user_id, :filename, :post_id])
    |> validate_required([:image_url, :filename, :user_id, :post_id])
  end

end
