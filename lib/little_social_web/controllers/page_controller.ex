defmodule LittleSocialWeb.PageController do
  use LittleSocialWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
