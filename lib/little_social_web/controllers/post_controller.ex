require IEx

defmodule LittleSocialWeb.PostController do
  use LittleSocialWeb, :controller

  alias LittleSocial.Images
  alias LittleSocial.Repo
  alias LittleSocial.Posts
  alias LittleSocial.Posts.Post
  alias LittleSocial.Posts.Comment
  alias LittleSocial.Accounts

  def index(conn, _params) do
    changeset = Posts.change_post(%Post{})
    posts = Posts.list_posts()
    render(conn, "index.html", posts: posts, changeset: changeset)
  end

  def new(conn, _params) do
    changeset = Posts.change_post(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    user_id = conn.assigns.current_user.id
    params = Map.merge(post_params, %{ "user_id" => user_id})
    case  Posts.create_post(params) do 
      {:ok, post} ->
       if post_params["image"] != nil do
          image_params = post_params["image"]
          Posts.create_image(post, image_params)
        end
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    user_id = conn.assigns.current_user.id
    comment_changeset = post
                        |> Ecto.build_assoc(:comments)
                        |> LittleSocial.Posts.Comment.changeset(%{post_id: post.id, user_id: user_id})
    render(conn, "show.html", post: post, user_id: user_id, comment_changeset: comment_changeset)
  end

  def edit(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    changeset = Posts.change_post(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Posts.get_post!(id)

    case Posts.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Posts.get_post!(id)
    {:ok, _post} = Posts.delete_post(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: Routes.post_path(conn, :index))
  end
end
