require IEx

defmodule LittleSocialWeb.CommentsController do
  use LittleSocialWeb, :controller

  alias LittleSocial.Repo
  alias LittleSocial.Posts.Comment
  alias LittleSocial.Posts.Post
  alias LittleSocial.Posts

  plug :scrub_params, "comment" when action in [:create, :update]

  def create(conn, %{"comment" => comment_params, "post_id" => post_id})  do
    params = Map.merge(comment_params, %{ "user_id" => conn.assigns.current_user.id, "post_id" => post_id }) 
    post = Posts.get_post!(post_id) 
    changeset = post
                |> Ecto.build_assoc(:comments) 
                |> Comment.changeset(params)
    case Repo.insert(changeset) do 
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment created!")
        |> redirect(to: Routes.post_path(conn, :show, post))
      {:error, changeset} ->
        render(conn, LittleSocial.PostView, "show.html", post: post, user: post.user, comment_changeset: changeset)
    end
  end

  def update(conn, _),  do: conn
  def delete(conn, _),  do: conn

end
