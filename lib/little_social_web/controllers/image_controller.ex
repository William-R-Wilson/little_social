require IEx

defmodule LittleSocialWeb.ImageController do
  use LittleSocialWeb, :controller 
  alias LittleSocial.Repo
  alias LittleSocial.Images
  alias LittleSocial.Images.Image 

  def new(conn, _params) do
    changeset = Images.change_image(%Image{})  
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"image" => image_params}) do
    user_id = conn.assigns.current_user.id
    case Images.create_image(image_params, user_id) do
    {:ok, image} ->
        conn
        |> put_flash(:info, "Image uploaded successfully!")
        |> redirect(to: Routes.image_path(conn, :new))
    {:error, changeset} ->
        render conn, "new.html", changeset: changeset
    end
  end

  def show(conn, %{"id" => id}) do
    image = Images.get_image!(id)
    render(conn, "show.html", image: image)
  end

end
