# LittleSocial

This is a work in progress, still in the early stages

LittleSocial is a bespoke social network.  I want to stay in touch with family and friends without the big social media companies 
 - manipulating what I see via algorithm
 - hiding content from friends, while showing me things I don't care about or want to see
 - allowing third parties to spread misinformation by manipulating the algorithm
 - collecting and monetizing data about me
 - leaking or selling this data to unscrupulous third parties 

LittleSocial will allow the basic features of a social network without any of the monetization or privacy issues.  

 - you will control who sees your posts
 - if you delete your account, all your content will be deleted
 - content will not be monetized.  No google ads or activity trackers

This program is Free and Open Source.  This means you can use my instance, or deploy your own.  

Individual communities can have their own instances, funded and controlled how they wish. 

You can fork this and add/modify functionality to suit your own needs, or contribute to this project.



To start locally:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * create an S3 bucket and add keys to .env file: https://www.poeticoding.com/aws-s3-in-elixir-with-exaws/
  * `source .env`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

#Deploy to Prod

`source .env` 

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).


#Todo

 - better design
 - better feed - display images
 - paginate feed
 - link preview           
 - image thumbnails - look into Mogrify and ImageMagick
 - image index          
 - add images to posts 
 - notifications      
 - friends           
 - deploy           

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
